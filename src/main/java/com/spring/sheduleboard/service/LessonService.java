package com.spring.sheduleboard.service;

import com.spring.sheduleboard.entity.Lesson;

import java.util.List;

public interface LessonService {
    Lesson getById(Long id);
    Lesson save(Lesson lesson);
    List<Lesson> getAllLessons();
}
