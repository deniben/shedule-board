package com.spring.sheduleboard.service.implementation;

import com.spring.sheduleboard.dao.LessonDao;
import com.spring.sheduleboard.entity.Lesson;
import com.spring.sheduleboard.service.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LessonServiceImpl implements LessonService {
    private LessonDao lessonDao;

    @Autowired
    public LessonServiceImpl(LessonDao lessonDao) {
        this.lessonDao = lessonDao;
    }

    @Override
    public Lesson getById(Long id) {
        return lessonDao.getById(id);
    }

    @Override
    public Lesson save(Lesson lesson) {
        return lessonDao.save(lesson);
    }

    @Override
    public List<Lesson> getAllLessons() {
        return lessonDao.getLessons();
    }
}
