package com.spring.sheduleboard.dao;

import com.spring.sheduleboard.entity.User;

import java.util.List;

public interface UserDao {
    User findById(Long id);

    void save(User user);

    List<User> findAll();
}
