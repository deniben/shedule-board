package com.spring.sheduleboard.dao;

import com.spring.sheduleboard.entity.Lesson;

import java.util.List;

public interface LessonDao {
    Lesson getById(Long id);
    Lesson save(Lesson lesson);
    List<Lesson> getLessons();
}
