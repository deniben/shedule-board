package com.spring.sheduleboard.dao.implementation;

import com.spring.sheduleboard.dao.AbstractDao;
import com.spring.sheduleboard.dao.UserDao;
import com.spring.sheduleboard.entity.User;

import java.util.List;

public class UserDaoImpl extends AbstractDao<Long, User> implements UserDao {
    @Override
    public User findById(Long id) {
        return getByKey(id);
    }

    @Override
    public void save(User user) {
        persist(user);
    }

    @Override
    public List<User> findAll() {
        return getEntityManager()
                .createQuery("select u from User u order by u.username asc")
                .getResultList();
    }
}
