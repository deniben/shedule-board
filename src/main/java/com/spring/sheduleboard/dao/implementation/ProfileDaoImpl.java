package com.spring.sheduleboard.dao.implementation;

import com.spring.sheduleboard.dao.AbstractDao;
import com.spring.sheduleboard.dao.ProfileDao;
import com.spring.sheduleboard.entity.Profile;

import java.util.List;

public class ProfileDaoImpl extends AbstractDao<Long, Profile> implements ProfileDao {
    @Override
    public Profile findById(Long id) {
        return getByKey(id);
    }

    @Override
    public void save(Profile profile) {
        persist(profile);
    }

    @Override
    public List<Profile> findAll() {
        return getEntityManager()
                .createQuery("select p from Profile p order by p.first_name asc ")
                .getResultList();
    }
}
