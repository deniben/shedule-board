package com.spring.sheduleboard.dao.implementation;

import com.spring.sheduleboard.dao.LessonDao;
import com.spring.sheduleboard.entity.Lesson;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class LessonDaoImpl implements LessonDao {

    private SessionFactory sessionFactory;

    @Autowired
    public LessonDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Lesson getById(Long id) {
        return sessionFactory.getCurrentSession().get(Lesson.class, id);
    }

    @Override
    public Lesson save(Lesson lesson) {
        sessionFactory.getCurrentSession().saveOrUpdate(lesson);
        return sessionFactory.getCurrentSession().createQuery("from Company c where c.id = :id", Lesson.class)
                .setParameter("id", lesson.getId())
                .getSingleResult();
    }

    @Override
    public List<Lesson> getLessons() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Lesson> cq = cb.createQuery(Lesson.class);
        Root<Lesson> root = cq.from(Lesson.class);
        cq.select(root);
        return session.createQuery(cq).getResultList();
    }
}
