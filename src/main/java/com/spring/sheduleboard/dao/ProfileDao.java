package com.spring.sheduleboard.dao;

import com.spring.sheduleboard.entity.Profile;

import java.util.List;

public interface ProfileDao {
    Profile findById(Long id);

    void save(Profile profile);

    List<Profile> findAll();
}
