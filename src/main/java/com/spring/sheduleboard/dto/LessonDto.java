package com.spring.sheduleboard.dto;

import com.spring.sheduleboard.entity.Group;

import java.time.DayOfWeek;

public class LessonDto {
    private Long id;
    private DayOfWeek dayOfWeek;
    private String firstLesson;
    private String secondLesson;
    private String thirdLesson;
    private String fourthLesson;
    private String fifthLesson;
    private String sixthLesson;
    private Long audience;
    private Group group;
    private Long building;

    public LessonDto() {
    }

    public LessonDto(Long id, DayOfWeek dayOfWeek,
                     String firstLesson, String secondLesson,
                     String thirdLesson, String fourthLesson,
                     String fifthLesson, String sixthLesson,
                     Long audience, Group group, Long building) {
        this.id = id;
        this.dayOfWeek = dayOfWeek;
        this.firstLesson = firstLesson;
        this.secondLesson = secondLesson;
        this.thirdLesson = thirdLesson;
        this.fourthLesson = fourthLesson;
        this.fifthLesson = fifthLesson;
        this.sixthLesson = sixthLesson;
        this.audience = audience;
        this.group = group;
        this.building = building;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getFirstLesson() {
        return firstLesson;
    }

    public void setFirstLesson(String firstLesson) {
        this.firstLesson = firstLesson;
    }

    public String getSecondLesson() {
        return secondLesson;
    }

    public void setSecondLesson(String secondLesson) {
        this.secondLesson = secondLesson;
    }

    public String getThirdLesson() {
        return thirdLesson;
    }

    public void setThirdLesson(String thirdLesson) {
        this.thirdLesson = thirdLesson;
    }

    public String getFourthLesson() {
        return fourthLesson;
    }

    public void setFourthLesson(String fourthLesson) {
        this.fourthLesson = fourthLesson;
    }

    public String getFifthLesson() {
        return fifthLesson;
    }

    public void setFifthLesson(String fifthLesson) {
        this.fifthLesson = fifthLesson;
    }

    public String getSixthLesson() {
        return sixthLesson;
    }

    public void setSixthLesson(String sixthLesson) {
        this.sixthLesson = sixthLesson;
    }

    public Long getAudience() {
        return audience;
    }

    public void setAudience(Long audience) {
        this.audience = audience;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Long getBuilding() {
        return building;
    }

    public void setBuilding(Long building) {
        this.building = building;
    }
}
