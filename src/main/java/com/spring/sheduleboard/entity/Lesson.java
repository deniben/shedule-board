package com.spring.sheduleboard.entity;

import javax.persistence.*;
import java.time.DayOfWeek;

@Entity
@Table(name = "study_day")
public class Lesson {

    @Id
    @SequenceGenerator(name = "study_day_sequence", sequenceName = "study_days_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "study_day_sequence")
    private Long id;

    @Enumerated(EnumType.STRING)
    private DayOfWeek dayOfWeek;

    @Column(name = "first_lesson")
    private String firstLesson;

    @Column(name = "second_lesson")
    private String secondLesson;

    @Column(name = "third_lesson")
    private String thirdLesson;

    @Column(name = "fourth_lesson")
    private String fourthLesson;

    @Column(name = "fifth_lesson")
    private String fifthLesson;

    @Column(name = "sixth_lesson")
    private String sixthLesson;

    @Column(name = "audience")
    private Long audience;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    @Column(name = "building")
    private Long building;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getFirstLesson() {
        return firstLesson;
    }

    public void setFirstLesson(String firstLesson) {
        this.firstLesson = firstLesson;
    }

    public String getSecondLesson() {
        return secondLesson;
    }

    public void setSecondLesson(String secondLesson) {
        this.secondLesson = secondLesson;
    }

    public String getThirdLesson() {
        return thirdLesson;
    }

    public void setThirdLesson(String thirdLesson) {
        this.thirdLesson = thirdLesson;
    }

    public String getFourthLesson() {
        return fourthLesson;
    }

    public void setFourthLesson(String fourthLesson) {
        this.fourthLesson = fourthLesson;
    }

    public String getFifthLesson() {
        return fifthLesson;
    }

    public void setFifthLesson(String fifthLesson) {
        this.fifthLesson = fifthLesson;
    }

    public String getSixthLesson() {
        return sixthLesson;
    }

    public void setSixthLesson(String sixthLesson) {
        this.sixthLesson = sixthLesson;
    }

    public Long getAudience() {
        return audience;
    }

    public void setAudience(Long audience) {
        this.audience = audience;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Long getBuilding() {
        return building;
    }

    public void setBuilding(Long building) {
        this.building = building;
    }
}
