package com.spring.sheduleboard.entity;

import javax.persistence.*;

@Entity
@Table(name = "groups")
public class Group {

    @Id
    @SequenceGenerator(name = "group_sequence" , sequenceName = "group_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "group_sequence")
    private Long id;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
