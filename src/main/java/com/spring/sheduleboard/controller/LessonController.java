package com.spring.sheduleboard.controller;

import com.spring.sheduleboard.dto.LessonDto;
import com.spring.sheduleboard.entity.Lesson;
import com.spring.sheduleboard.service.LessonService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lessons")
public class LessonController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LessonController.class);
    private LessonService lessonService;
    private ModelMapper modelMapper;

    @Autowired
    public LessonController(LessonService lessonService, ModelMapper modelMapper) {
        this.lessonService = lessonService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public ResponseEntity<List> getAllLessons() {
        return ResponseEntity.ok(lessonService.getAllLessons());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Lesson> getLessonById(@PathVariable Long id) {
        return ResponseEntity.ok(lessonService.getById(id));
    }

    @PostMapping
    public ResponseEntity createLesson(@RequestBody LessonDto lessonDto) {
        LOGGER.debug("in createLesson({})", lessonDto);
        return ResponseEntity.ok(modelMapper.map(lessonService.save(modelMapper.map(lessonDto, Lesson.class)), LessonDto.class));
    }

}
